﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeMove : MonoBehaviour
{

    public Rigidbody2D slimeRB;
    
    public Transform[] slimePositions = new Transform[2];
    public float slimeSpeed;

    private int actualPosition = 0;
    private int nextPosition = 1;

    public bool moveToTheNext = true;
    public static bool isAlive = true;
    public float waitTime = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            MoveSlime();
        }
    }
    
    void MoveSlime()
    {
        if (moveToTheNext)
        {
            slimeRB.MovePosition(Vector3.MoveTowards(slimeRB.position, slimePositions[nextPosition].position, slimeSpeed * Time.deltaTime));
            
            if (Vector3.Distance(slimeRB.position, slimePositions[nextPosition].position) <= 0)
            {
                StartCoroutine(WaitForMove());
                actualPosition = nextPosition;
                nextPosition++;

                if (nextPosition > slimePositions.Length - 1)
                {
                    nextPosition = 0;
                }
            }
        }

    }

    IEnumerator WaitForMove()
    {
        moveToTheNext = false;
        yield return new WaitForSeconds(waitTime);
        moveToTheNext = true;
    }
    
}
